import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  displayedColumns :string[]=[
    'select',
    'image',
    'size',
    'recognition',
    'download'
  ];

  dataSource:string[][]=[
    ['Dog','25','rec','www.dog.com'],
    ['Cat','50','rec','www.cat.com'],
    ['Cow','75','rec','www.cow.com'],
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
