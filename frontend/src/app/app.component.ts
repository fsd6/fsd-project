import { Component } from '@angular/core';
import { HeaderStateService } from './Services/header-state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Aplicatie';

  constructor(public headerState:HeaderStateService) {
    this.headerState.showHeader=false;
   }
}
