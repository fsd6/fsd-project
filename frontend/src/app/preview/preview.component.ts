import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

  constructor(private http: HttpClient) { }

  result:any;
  ngOnInit(): void {
    this.get();
    //console.log(this.result);
  }

   get(){
     this.http.get("https://us-central1-fsd-cloud.cloudfunctions.net/helloWorld").subscribe((rezultat:any) =>{
      this.result=rezultat.msg;
    })
  }


}
