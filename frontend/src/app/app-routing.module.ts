import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component'
import {HistoryComponent} from './history/history.component'
import { UploadComponent } from './upload/upload.component';
import {PreviewComponent} from './preview/preview.component'
const routes: Routes = [
  {path:"", redirectTo:"preview" , pathMatch:"full"},
  {path:"login", component:LoginComponent},
  {path:"history", component:HistoryComponent},
  {path:"upload",component:UploadComponent},
  {path:"preview",component:PreviewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
