import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {HeaderStateService} from '../Services/header-state.service'
import {UserService} from '../Services/user.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  profileForm = new FormGroup({
    username: new FormControl('',[Validators.required, Validators.minLength(8)]),
    password: new FormControl('',[Validators.required, Validators.minLength(8)]),
  });

  constructor(public headerState:HeaderStateService, public userService: UserService, private router: Router) {
    this.headerState.showHeader=false;
   }

  ngOnInit(): void {
  }

  login() {
    console.log(this.profileForm.valid);
    if(this.profileForm.valid){
      this.userService.login(this.profileForm.getRawValue())
        .subscribe(
      data=> {
        console.log(data);
        localStorage.setItem('token', data.toString());
        this.router.navigate(['/history']);
      },
      error=>{}
      /*success => {
      if (success) {
        this.headerState.showHeader=true;
        this.router.navigate(['/history']);*/
    );
  
  }
  }

}
