import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';
import { Component, OnInit } from '@angular/core';
import{HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  selectedFile=null;
  constructor(private http: HttpClient) { }
  url="assets/image/image.png"
  onFileSelected(event){
    if(event.target.files){
      var reader=new FileReader()
      reader.readAsDataURL(event.target.files[0])
      reader.onload=(event: any) => {
        this.url=event.target.result
      }
    }
  }
  ngOnInit(): void {
  }

}
