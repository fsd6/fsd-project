import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestRequestService {
  baseUrl="http://localhost:3000";

  constructor(private http: HttpClient){}

    post(user: { username: string, password: string }, url:string): Observable<any> {
      const token = localStorage.getItem('token');
      const headers = new HttpHeaders().set('Authorization', "Bearer " + token);
      return this.http.post(this.baseUrl + url, user, {headers: headers});
       /* const event = new EventEmitter();

        setTimeout(() => {
          event.emit({myObj: {}})          
        }) 
        return event;*/
      }

    get(url: string): Observable<any> {
        return this.http.get(this.baseUrl + url);
      }
}
