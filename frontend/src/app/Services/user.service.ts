import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestRequestService } from './rest-request.service';
import {User} from '../Models/user'
import {tap} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userData:User;

  constructor(public restRequestService: RestRequestService) { }

  login(user: { username: string, password: string }): Observable<User> {
    return this.restRequestService.post(user, '/login')
      .pipe(
        tap(
          data =>{ this.userData=data}
        ));
  }
}
