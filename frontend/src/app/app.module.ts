import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field'
import {MatIconModule} from '@angular/material/icon'
import {MatButtonModule} from '@angular/material/button'
import {MatInputModule} from '@angular/material/input'
import{ FlexLayoutModule} from '@angular/flex-layout'
import { MatCardModule} from '@angular/material/card'
import {MatGridListModule} from '@angular/material/grid-list';
import { HistoryComponent } from './history/history.component'
import {MatTableModule} from '@angular/material/table'
import {MatCheckboxModule} from '@angular/material/checkbox';
import { UploadComponent } from './upload/upload.component'
import{HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms'
import { UserService } from './Services/user.service';
import { PreviewComponent } from './preview/preview.component';
import {Datastore} from '@google-cloud/datastore';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HistoryComponent,
    UploadComponent,
    PreviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    FlexLayoutModule,
    MatCardModule,
    MatGridListModule,
    MatTableModule,
    MatCheckboxModule,
    HttpClientModule,
    ReactiveFormsModule,
    Datastore
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
