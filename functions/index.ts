export function helloWorld(req, res){
    res.set({'Access-Control-Allow-Origin': '*'});
    if(req.method === 'OPTIONS'){
        res.set('Access-Control-Allow-Methods','GET');
        res.set('Access-Control-Allow-Headers','Content-Type');
        res.set('Access-Control-Max-Age','3600');
        res.status(204).send('');
    }else{
        res.send({msg: 'Hello World!'});
    }
   
  };

  const Datastore = require('@google-cloud/datastore');
  const datastore = new Datastore({
      projectId: 'fsd-project-310916',
      keyFilename: 'datastore-credential.json'
  });
  const kindName = 'user-log';
  exports.savelog = (req, res) => {
      let uid = req.query.uid || req.body.uid || 0;
      let log = req.query.log || req.body.log || '';
      datastore
          .save({
              key: datastore.key(kindName),
              data: {
                  log: log,
                  uid: datastore.int(uid),
                  time_create: datastore.int(Math.floor(new Date().getTime()/1000))
              }
          })
          .catch(err => {
              console.error('ERROR:', err);
              res.status(200).send(err);
              return;
          });
      res.status(200).send(log);
  };