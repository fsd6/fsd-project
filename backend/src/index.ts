import "reflect-metadata";
import {createConnection, getRepository} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";
import {User} from "./entity/User";
import config from "./config/config";
import {cors} from "cors";
var jwt = require('express-jwt');

const fileUpload = require('express-fileupload')

createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.json());
    app.use(fileUpload({
        useTempFiles: false,
        tempFileDir: '/tmp'
    }));
    var cors = require('cors')
    
    app.use(cors());

    // register express routes from defined application routes
    Routes.forEach(route => {
        console.log(route.action);
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    // insert new users for test
    /*await connection.manager.save(connection.manager.create(User, {
        firstName: "Timber",
        lastName: "Saw",
        age: 27
    }));
    await connection.manager.save(connection.manager.create(User, {
        firstName: "Phantom",
        lastName: "Assassin",
        age: 24
    }));*/

    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");


    /*
    var router=express.Router();
    router.post('/login',function(req,res,next){
        
        const userRepository = getRepository(User);
        let promise= userRepository.findOne({username:req.body.username});
        
        promise.then(function(doc){
            if(doc){
                if(doc.checkIfUnencryptedPasswordIsValid(req.body.password)){
                    let token=jwt.sign({username:doc.username},'secret',{expiresIn: '12h'});
                    return res.status(200).json(token);
                }else {
                    return res.status(501).json({message:'Invalid credentials'});
                }
            }
            else{
                return res.status(501).json({message:'Invalid username'});
            }
        });
        promise.catch(function(err){
            return res.status(501).json({message:"Error"});
        })
   
    })*/
    app.use(jwt({
        secret: config.jwtSecret,
        algorithms: ['HS256'],
        credentialsRequired: false,
      }));

      /*jwt.verify(token, 'shhhhh', function(err, decoded) {
        console.log(decoded.foo); // bar
        Response.writeHead(404, "{Not found}");
        Response.write("404 Not found");
        Response.end();
      });*/


}).catch(error => console.log(error));
