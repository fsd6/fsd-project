import AuthController from "./controller/AuthController";
import {UserController} from "./controller/UserController";
import {TableItemController} from "./controller/TableItemController"
import {ImageController} from "./controller/ImageController"

export const Routes = [{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all"
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one"
}, {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save"
}, {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove"
}, {
    method: "post",
    route: "/login",
    controller: AuthController,
    action: "login"
}, {
    method: "get",
    route: "/history",
    controller: TableItemController,
    action: "all"
}, {
    method: "get",
    route: "/evaluate",
    controller: ImageController,
    action: "evaluate"
}, {
    method: "post",
    route: "/upload",
    controller: ImageController,
    action: "upload"
}];