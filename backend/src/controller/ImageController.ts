import {NextFunction, Request, Response} from "express"
import {MnistData} from '../data';
import * as tf from "@tensorflow/tfjs-node"

export class ImageController{
    classNames = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];

    doPrediction(model, data, testDataSize = 500) {
    const IMAGE_WIDTH = 28;
    const IMAGE_HEIGHT = 28;
    const testData = data.nextTestBatch(testDataSize);
    const testxs = testData.xs.reshape([testDataSize, IMAGE_WIDTH, IMAGE_HEIGHT, 1]);
    const labels = testData.labels.argMax(-1);
    const preds = model.predict(testxs).argMax(-1);

    testxs.dispose();
    return [preds, labels];
    }

    async evaluate(request: Request, response: Response, next: NextFunction){
        const model = await tf.loadLayersModel('file://./src/model.json');
        const data = new MnistData();
        await data.load(request.files.img);

        this.doPrediction(model,data, 1);
    }

    async upload(request: Request, response: Response, next: NextFunction){
        const model = await tf.loadLayersModel('file://./src/model.json');
        let tensor = tf.node.decodePng(request.files.image.data, 1)
        tensor = tensor.reshape([-1,28,28,1])
        let preds = model.predict(tensor) as tf.Tensor;
        preds = preds.argMax(-1);
        let tensorData = preds.dataSync();
        return tensorData[0].toString();
    }

    toArrayBuffer(buf){
        var ab= new ArrayBuffer(buf.lenght);
        var view = new Uint8Array(ab);
        for(var i =0 ; i< buf.lenght; i++){
            view[i] = buf [i];
        }
        return ab;
    }
}