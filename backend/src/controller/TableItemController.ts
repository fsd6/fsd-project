import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {TableItem} from "../entity/TableItem";
import {MnistData} from '../data';
import * as tf from "@tensorflow/tfjs-node"

export class TableItemController {

    private tabelItemRepository = getRepository(TableItem);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.tabelItemRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.tabelItemRepository.findOne(request.params.id);
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.tabelItemRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let itemToRemove = await this.tabelItemRepository.findOne(request.params.id);
        await this.tabelItemRepository.remove(itemToRemove);
    }


}